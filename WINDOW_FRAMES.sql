WITH "daily_sales" AS (
SELECT
EXTRACT ('week' FROM "sh"."times"."time_1d"):: INTEGER AS "calendar_week_number" ,
"sh"."times"."time_id",
"sh"."times"."day_name",
"sh"."times"."day_number_in_week",
SUM ("sh"."sales"."amount_sold") AS "daily_sales"
FROM "sh"."sales"
JOIN "sh"."channels" ON "sh"."sales"."channel_id" = "sh"."channels"."channel_id"
JOIN "sh"."times" ON "sh"."sales"."time_id" = "sh"."times"."time_id"
WHERE "sh"."times"."calendar_year" = 1999
AND EXTRACT ( 'week' FROM "sh"."times"."time_1d"):: INTEGER IN (49, 50, 51)
GROUP BY "sh"."times"."day_name", "sh"."times"."day_number_in_week", "sh"."times"."time_id"
ORDER BY EXTRACT ('week' FROM "sh"."times"."time_id"), "sh"."times"."day_number_in_week"
)
SELECT
"calendar_week_number",
"time_id",
"day_name",
"daily_sales" AS "sales",
SUM("daily_sales") OVER (ORDER BY "day_number_in_week", "time_id" ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW) AS "cum_sum",
ROUND ( case 
	WHEN "day_name" = 'Monday' THEN AVG("daily_sales") OVER (ORDER BY "day_number_in_week", "time_id" ROWS BETWEEN 2 PRECEDING AND 1 FOLLOWING)
	WHEN "day_name" = 'Friday' THEN AVG ("daily_sales") OVER (ORDER BY "day_number_in_week", "time_id" ROWS BETWEEN 1 PRECEDING AND 2 FOLLOWING)
	ELSE AVG ("daily_sales") OVER (ORDER BY "day_number_in_week", "time_id" ROWS BETWEEN 1 PRECEDING AND 1 FOLLOWING)
end, 2) as "centered_3_day_avg"
FROM "daily_sales";
